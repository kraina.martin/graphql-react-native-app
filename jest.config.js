module.exports = {
  preset: 'react-native',
  testMatch: ['<rootDir>/**/*.test.(t|j)s?(x)'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testPathIgnorePatterns: ['/node_modules/', '/e2e/'],
  setupFiles: ['<rootDir>/jest.setup.js', "./node_modules/react-native-gesture-handler/jestSetup.js"],
  cacheDirectory: '.jest/cache',
};
