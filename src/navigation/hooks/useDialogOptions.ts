import { useMemo } from 'react';
import { StackNavigationOptions } from '@react-navigation/stack';
import { useTheme } from 'styled-components/native';

const useDialogOptions = (): StackNavigationOptions => {
  const { isDark } = useTheme();
  return useMemo(
    () => ({
      headerShown: false,
      cardStyle: { backgroundColor: 'transparent' },
      cardOverlayEnabled: true,
      gestureEnabled: false,
      detachPreviousScreen: false,
      cardStyleInterpolator: props => ({
        cardStyle: {
          opacity: props.current.progress.interpolate({
            inputRange: [0, 0.5, 0.9, 1],
            outputRange: [0, 0.25, 0.7, 1],
            extrapolate: 'clamp',
          }),
        },
        overlayStyle: {
          opacity: props.current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [0, isDark ? 0.85 : 0.75],
            extrapolate: 'clamp',
          }),
        },
      }),
    }),
    [isDark]
  );
};
export default useDialogOptions;
