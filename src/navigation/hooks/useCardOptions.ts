import { useMemo } from 'react';
import { Animated, ViewStyle } from 'react-native';
import {
  StackCardInterpolatedStyle,
  StackCardInterpolationProps,
  StackNavigationOptions,
} from '@react-navigation/stack';
import { TransitionSpec } from '@react-navigation/stack/lib/typescript/src/types';
import { useTheme } from 'styled-components/native';

import { elevation } from '~src/ui';

const transitionSpec: TransitionSpec = {
  animation: 'spring',
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 10,
    restSpeedThreshold: 10,
  },
};

const horizontalInterpolator = (props: StackCardInterpolationProps): StackCardInterpolatedStyle => {
  const translateFocused = Animated.multiply(
    props.current.progress.interpolate({
      inputRange: [0, 1],
      outputRange: [props.layouts.screen.width, 0],
      extrapolate: 'clamp',
    }),
    props.inverted
  );

  const translateUnfocused = props.next
    ? Animated.multiply(
        props.next.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, props.layouts.screen.width * -0.3],
          extrapolate: 'clamp',
        }),
        props.inverted
      )
    : 0;

  const overlayOpacity = props.current.progress.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 0.7],
    extrapolate: 'clamp',
  });

  const shadowOpacity = props.current.progress.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 0.3],
    extrapolate: 'clamp',
  });

  return {
    cardStyle: {
      transform: [
        // Translation for the animation of the current card
        { translateX: translateFocused },
        // Translation for the animation of the card on top of this
        { translateX: translateUnfocused },
      ],
    },
    overlayStyle: { opacity: overlayOpacity },
    shadowStyle: { shadowOpacity },
  };
};

const useCardOptions = (): StackNavigationOptions => {
  const { colors } = useTheme();
  return useMemo(
    () => ({
      headerShown: false,
      cardStyle: {
        flex: 1,
        backgroundColor: colors.background,
        ...(elevation(14, true) as ViewStyle),
        overflow: 'visible',
      },
      transitionSpec: {
        open: transitionSpec,
        close: transitionSpec,
      },
      gestureDirection: 'horizontal',
      cardStyleInterpolator: horizontalInterpolator,
      gestureEnabled: true,
    }),
    [colors.background]
  );
};

export default useCardOptions;
