import React from 'react';
import RNBootSplash from 'react-native-bootsplash';
import { NavigationContainer } from '@react-navigation/native';

const onReady = () => {
  void RNBootSplash.hide({ fade: true });
};

export const NavigationProvider = React.memo(({ children }) => {
  return <NavigationContainer onReady={onReady}>{children}</NavigationContainer>;
});
