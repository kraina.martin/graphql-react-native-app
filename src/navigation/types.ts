import { BottomTabNavigationEventMap } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import { NavigationHelpers, RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export type TabStackParamList = {
  characterDetail: { id: string };
  characterList: undefined;
  episodeDetail: { id: string };
  episodeList: undefined;
  locationDetail: { id: string };
  locationList: undefined;
};

export type MainTabParamList = {
  characters: undefined;
  episodes: undefined;
  locations: undefined;
};

export type AppParamList = MainTabParamList & TabStackParamList;

export type AppScreenName = keyof AppParamList;

export type AppNavigationProps<S extends AppScreenName> = StackNavigationProp<AppParamList, S>;

export type AppRouteProps<T extends AppScreenName> = RouteProp<AppParamList, T>;

export type AppScreenProps<T extends AppScreenName> = {
  navigation: AppNavigationProps<T>;
  route: AppRouteProps<T>;
};

export type MainTabBarNavigation = NavigationHelpers<AppParamList, BottomTabNavigationEventMap>;
export type TabStackNavigationProp<T extends keyof TabStackParamList> = StackNavigationProp<
  AppParamList,
  T
>;

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace ReactNavigation {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface RootParamList extends AppParamList {}
  }
}
