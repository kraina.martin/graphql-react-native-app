import { Platform } from 'react-native';

const isIPhone = Platform.OS === 'ios';
const osVersion = isIPhone ? parseFloat(Platform.Version as string) : (Platform.Version as number);

export default {
  isIPhone,
  osVersion,
  isStaging: true,
};
