import React from 'react';

import { ApolloProvider } from '~src/api';
import { NavigationProvider } from '~src/navigation';
import { UiProvider } from '~src/ui';

import { MainTab } from './containers';

export default React.memo(() => (
  <ApolloProvider>
    <UiProvider>
      <NavigationProvider>
        <MainTab />
      </NavigationProvider>
    </UiProvider>
  </ApolloProvider>
));
