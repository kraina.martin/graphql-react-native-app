import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { TabStackParamList, useCardOptions } from '~src/navigation';

import CharacterDetailScreen from '../screens/CharacterDetailScreen';
import CharacterListScreen from '../screens/CharacterListScreen';
import EpisodesDetailScreen from '../screens/EpisodesDetailScreen';
import EpisodesListScreen from '../screens/EpisodesListScreen';
import LocationDetailScreen from '../screens/LocationDetailScreen';
import LocationListScreen from '../screens/LocationListScreen';

const Stack = createStackNavigator<TabStackParamList>();

const TabStack = React.memo<{ initialScreen: keyof TabStackParamList }>(({ initialScreen }) => {
  return (
    <Stack.Navigator initialRouteName={initialScreen} screenOptions={useCardOptions()}>
      <Stack.Screen name="characterList" component={CharacterListScreen} />
      <Stack.Screen name="characterDetail" component={CharacterDetailScreen} />
      <Stack.Screen name="episodeList" component={EpisodesListScreen} />
      <Stack.Screen name="episodeDetail" component={EpisodesDetailScreen} />
      <Stack.Screen name="locationList" component={LocationListScreen} />
      <Stack.Screen name="locationDetail" component={LocationDetailScreen} />
    </Stack.Navigator>
  );
});

export default TabStack;
