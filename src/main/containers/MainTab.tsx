import React from 'react';
import {
  BottomTabBarProps,
  BottomTabNavigationOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';

import { MainTabParamList } from '~src/navigation';

import TabBar, { TabBarProps } from '../components/TabBar';

import TabStack from './TabStack';

const screenOptions: BottomTabNavigationOptions = { lazy: false, headerShown: false };

const renderTabBar = (props: BottomTabBarProps) => (
  <TabBar {...(props as unknown as TabBarProps)} />
);

const Tab = createBottomTabNavigator<MainTabParamList>();

const CharactersTab = React.memo(() => <TabStack initialScreen="characterList" />);
const EpisodesTab = React.memo(() => <TabStack initialScreen="episodeList" />);
const LocationsTab = React.memo(() => <TabStack initialScreen="locationList" />);

const MainTab = React.memo(() => {
  return (
    <Tab.Navigator screenOptions={screenOptions} tabBar={renderTabBar}>
      <Tab.Screen name="characters" component={CharactersTab} />
      <Tab.Screen name="episodes" component={EpisodesTab} />
      <Tab.Screen name="locations" component={LocationsTab} />
    </Tab.Navigator>
  );
});

export default MainTab;
