import styled from 'styled-components/native';

import { elevation, H2, Touchable } from '~src/ui';

export const Wrapper = styled(Touchable)<{
  showShadow?: boolean;
}>(({ showShadow, theme: { colors, isDark } }) => ({
  backgroundColor: isDark ? colors.background01 : colors.background,
  ...elevation(showShadow ? 4 : 0),
  zIndex: 100,
}));

export const Content = styled.View({
  height: 60,
  flexDirection: 'row-reverse',
  alignItems: 'center',
  justifyContent: 'space-between',
});

export const ChildrenWrapper = styled.View({
  paddingHorizontal: 4,
  flexDirection: 'row-reverse',
  alignItems: 'center',
  flex: 1,
});

export const Label = styled(H2)<{ withBackButton: boolean }>(({ withBackButton }) => ({
  marginLeft: withBackButton ? 0 : 4,
}));

export const LabelWrapper = styled.View<{ withBackButton: boolean }>(({ withBackButton }) => ({
  marginLeft: withBackButton ? 0 : 4,
  flex: 1,
  alignItems: 'flex-start',
  justifyContent: 'center',
}));
