import React from 'react';
import { useIsFocused, useNavigation } from '@react-navigation/native';

import { useIsFirstInStack } from '~src/navigation';
import { HeaderButton, SafeArea, StatusBar } from '~src/ui';

import * as S from './styled';

export interface HeaderProps {
  onPress: () => void;
  absolute?: boolean;
  label?: string | React.ReactNode;
}

const Header = React.memo<HeaderProps>(props => {
  const isFocused = useIsFocused();
  const isFirstInStack = useIsFirstInStack();
  const hasBackButton = !isFirstInStack;
  const { goBack } = useNavigation();

  return (
    <S.Wrapper showShadow={!props.absolute} onPress={props.onPress} noFeedback>
      <SafeArea.Top />
      {isFocused && <StatusBar />}
      <S.Content>
        <S.ChildrenWrapper>
          <S.LabelWrapper withBackButton={hasBackButton}>
            {typeof props.label === 'string' ? (
              <S.Label color="foreground" withBackButton={hasBackButton} numberOfLines={2}>
                {props.label}
              </S.Label>
            ) : (
              props.label
            )}
          </S.LabelWrapper>
        </S.ChildrenWrapper>
        {hasBackButton && <HeaderButton icon="chevron-left" onPress={goBack} />}
      </S.Content>
    </S.Wrapper>
  );
});

export default Header;
