import React, { useCallback } from 'react';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import { TabNavigationState } from '@react-navigation/native';
import { useTheme } from 'styled-components/native';

import { MainTabBarNavigation, MainTabParamList } from '~src/navigation';
import { H4, IconName, NavigationBar, SafeArea } from '~src/ui';
import { Theme } from '~src/ui/types';

import * as S from './styled';

export type TabBarProps = Omit<BottomTabBarProps, 'navigation' | 'state'> & {
  navigation: MainTabBarNavigation;
  state: TabNavigationState<MainTabParamList>;
};

const iconMap: Record<keyof MainTabParamList, IconName> = {
  characters: 'list',
  episodes: 'tv',
  locations: 'map-pin',
};

const labelsMap: Record<keyof MainTabParamList, string> = {
  characters: 'Characters',
  episodes: 'Episodes',
  locations: 'Locations',
};

const Button = React.memo<{
  isFocused: boolean;
  navigation: MainTabBarNavigation;
  route: TabBarProps['state']['routes'][0];
}>(({ isFocused, route, navigation: { emit, navigate } }) => {
  const color: keyof Theme['colors'] = isFocused ? 'foreground' : 'foreground02';
  return (
    <S.ButtonWrapper
      onPress={useCallback(() => {
        const event = emit({
          type: 'tabPress',
          target: route.key,
          canPreventDefault: true,
        });

        if (!isFocused && !event.defaultPrevented) {
          navigate(route);
        }
      }, [emit, isFocused, navigate, route])}
    >
      <S.ButtonIcon size={24} name={iconMap[route.name]} color={color} />
      <H4 color={color} numberOfLines={1}>
        {labelsMap[route.name]}
      </H4>
    </S.ButtonWrapper>
  );
});

const MainTabBar = React.memo<TabBarProps>(props => {
  const { state, navigation } = props;
  const { colors, isDark } = useTheme();
  return (
    <S.Wrapper>
      <S.Content accessibilityRole="tablist">
        {state.routes.map((route, index) => (
          <Button
            navigation={navigation}
            route={route}
            key={route.key}
            isFocused={state.index === index}
          />
        ))}
      </S.Content>
      <SafeArea.Bottom />
      <NavigationBar color={isDark ? colors.background01 : colors.background} />
    </S.Wrapper>
  );
});

export default MainTabBar;
