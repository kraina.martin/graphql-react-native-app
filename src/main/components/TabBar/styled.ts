import styled from 'styled-components/native';

import { Color, elevation, Icon, Touchable } from '~src/ui';

export const Wrapper = styled.View(({ theme: { colors, isDark } }) => ({
  backgroundColor: isDark ? colors.background01 : colors.background,
  borderTopWidth: 1,
  borderColor: Color.alpha(colors.foreground02, 0.2),
  ...elevation(10),
}));

export const Content = styled.View({
  flexDirection: 'row',
  height: 58,
});

export const ButtonWrapper = styled(Touchable)({
  alignItems: 'center',
  flex: 1,
  justifyContent: 'center',
});

export const ButtonIcon = styled(Icon)({ padding: 4 });
