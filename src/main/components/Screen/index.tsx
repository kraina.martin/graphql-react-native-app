import React, { useCallback, useRef } from 'react';
import { FlatList, ScrollView, ScrollViewProps } from 'react-native';

import { List, ListItemBase, ListProps } from '~src/ui';

import Header from '../Header';

import * as S from './styled';

type BaseProps = {
  title?: string | React.ReactNode;
};

type StaticProps = {
  children: React.ReactNode;
  type?: 'static';
} & ScrollViewProps;

type Props<T extends ListItemBase> = BaseProps &
  (StaticProps | ({ type: 'list' } & Omit<ListProps<T>, 'style'>));

const Screen = React.memo<Props<ListItemBase>>(props => {
  const scrollRef = useRef<ScrollView>(null);
  const listRef = useRef<FlatList>(null);
  return (
    <S.Wrapper>
      <Header
        label={props.title}
        onPress={useCallback(() => {
          listRef.current?.scrollToOffset({ offset: 0 });
          scrollRef.current?.scrollTo({ y: 0 });
        }, [])}
      />
      {props.type === 'list' ? (
        <List ref={listRef} {...props} />
      ) : (
        <ScrollView ref={scrollRef} {...props} />
      )}
    </S.Wrapper>
  );
});

export default Screen as <T extends ListItemBase>(props: Props<T>) => React.ReactElement;
