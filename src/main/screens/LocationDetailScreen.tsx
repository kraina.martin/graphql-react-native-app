import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';

import { LocationDetail_location_residents, useLocationDetail } from '~src/locations';
import { AppScreenProps, TabStackNavigationProp } from '~src/navigation';
import { Body } from '~src/ui';

import { Screen } from '../components';

const LocationProp = React.memo<{
  children: string | null | undefined | unknown;
}>(props => {
  if (typeof props.children === 'string') return <Body color="foreground">{props.children}</Body>;
  if (typeof props.children === 'object' && props.children)
    return (
      <>
        {Object.entries(props.children).map(([key, val]) => (
          <LocationProp key={key}>{val}</LocationProp>
        ))}
      </>
    );
  return null;
});

const Resident = React.memo<LocationDetail_location_residents>(props => {
  const { push } = useNavigation<TabStackNavigationProp<'locationDetail'>>();
  return (
    <Body
      color="foreground"
      onPress={useCallback(() => push('characterDetail', { id: props.id! }), [props.id, push])}
    >
      {props.name}
    </Body>
  );
});

export default React.memo<AppScreenProps<'locationDetail'>>(
  ({
    route: {
      params: { id },
    },
  }) => {
    const locationDetail = useLocationDetail(id);
    const { residents, name, ...location } = locationDetail.data?.location || {};
    return (
      <Screen title={name}>
        <LocationProp>{location}</LocationProp>
        <Body color="foreground">Residents</Body>
        {residents?.map(e => e && <Resident key={e.id} {...e} />)}
      </Screen>
    );
  }
);
