import React, { useMemo } from 'react';
import { ListRenderItem } from 'react-native';

import { LocationCard, LocationList_locations_results, useLocationList } from '~src/locations';
import { Logo } from '~src/ui';

import { Screen } from '../components';

const renderItem: ListRenderItem<LocationList_locations_results> = ({ item }) => (
  <LocationCard {...item} />
);

export default React.memo(() => {
  const { data, loading, refetch, hasMore, fetchMore } = useLocationList();
  return (
    <Screen
      title={useMemo(
        () => (
          <Logo horizontal height={48} />
        ),
        []
      )}
      type="list"
      refreshing={loading}
      onRefresh={refetch}
      data={data?.locations?.results}
      renderItem={renderItem}
      hasMore={hasMore}
      onEndReached={fetchMore}
    />
  );
});
