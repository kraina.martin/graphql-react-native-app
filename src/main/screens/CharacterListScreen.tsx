import React, { useMemo } from 'react';
import { ListRenderItem } from 'react-native';

import { CharacterList_characters_results, useCharacterList } from '~src/characters';
import { CharacterCard } from '~src/characters/components';
import { Logo } from '~src/ui';

import { Screen } from '../components';

const renderItem: ListRenderItem<CharacterList_characters_results> = ({ item }) => (
  <CharacterCard {...item} />
);

export default React.memo(() => {
  const { data, hasMore, loading, refetch, fetchMore } = useCharacterList();
  return (
    <Screen
      title={useMemo(
        () => (
          <Logo horizontal height={48} />
        ),
        []
      )}
      type="list"
      refreshing={loading}
      onRefresh={refetch}
      data={data?.characters?.results}
      renderItem={renderItem}
      hasMore={hasMore}
      onEndReached={fetchMore}
    />
  );
});
