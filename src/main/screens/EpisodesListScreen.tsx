import React, { useMemo } from 'react';
import { ListRenderItem } from 'react-native';

import { EpisodeCard, EpisodeList_episodes_results, useEpisodeList } from '~src/episodes';
import { Logo } from '~src/ui';

import { Screen } from '../components';

const renderItem: ListRenderItem<EpisodeList_episodes_results> = ({ item }) => (
  <EpisodeCard {...item} />
);

export default React.memo(() => {
  const { data, loading, refetch, hasMore, fetchMore } = useEpisodeList();
  return (
    <Screen
      title={useMemo(
        () => (
          <Logo horizontal height={48} />
        ),
        []
      )}
      type="list"
      refreshing={loading}
      onRefresh={refetch}
      data={data?.episodes?.results}
      renderItem={renderItem}
      hasMore={hasMore}
      onEndReached={fetchMore}
    />
  );
});
