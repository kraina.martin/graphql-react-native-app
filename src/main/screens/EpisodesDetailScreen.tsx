import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';

import { EpisodeDetail_episode_characters } from '~src/episodes';
import { useEpisodeDetail } from '~src/episodes/queries/useEpisodeDetail';
import { AppScreenProps, TabStackNavigationProp } from '~src/navigation';
import { Body } from '~src/ui';

import { Screen } from '../components';

const EpisodeProp = React.memo<{
  children: string | null | undefined | unknown;
}>(props => {
  if (typeof props.children === 'string') return <Body color="foreground">{props.children}</Body>;
  if (typeof props.children === 'object' && props.children)
    return (
      <>
        {Object.entries(props.children).map(([key, val]) => (
          <EpisodeProp key={key}>{val}</EpisodeProp>
        ))}
      </>
    );
  return null;
});

const Episode = React.memo<EpisodeDetail_episode_characters>(props => {
  const { push } = useNavigation<TabStackNavigationProp<'episodeDetail'>>();
  return (
    <Body
      color="foreground"
      onPress={useCallback(() => push('characterDetail', { id: props.id! }), [props.id, push])}
    >
      {props.name}
    </Body>
  );
});

export default React.memo<AppScreenProps<'episodeDetail'>>(
  ({
    route: {
      params: { id },
    },
  }) => {
    const episodeDetail = useEpisodeDetail(id);
    const { characters, name, ...episode } = episodeDetail.data?.episode || {};
    return (
      <Screen title={name}>
        <EpisodeProp>{episode}</EpisodeProp>
        <Body color="foreground">Characters</Body>
        {characters?.map(e => e && <Episode key={e.id} {...e} />)}
      </Screen>
    );
  }
);
