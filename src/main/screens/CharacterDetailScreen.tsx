import React, { useCallback } from 'react';
import { Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { CharacterEpisodes_character_episode, useCharacterDetail } from '~src/characters';
import { useCharacterEpisodes } from '~src/characters/queries/useCharacterEpisodes';
import { AppScreenProps, TabStackNavigationProp } from '~src/navigation';
import { Body } from '~src/ui';

import { Screen } from '../components';

const CharacterProp = React.memo<{
  children: string | null | undefined | unknown;
}>(props => {
  if (typeof props.children === 'string') return <Body color="foreground">{props.children}</Body>;
  if (typeof props.children === 'object' && props.children)
    return (
      <>
        {Object.entries(props.children).map(([key, val]) => (
          <CharacterProp key={key}>{val}</CharacterProp>
        ))}
      </>
    );
  return null;
});

const Episode = React.memo<CharacterEpisodes_character_episode>(props => {
  const { push } = useNavigation<TabStackNavigationProp<'characterDetail'>>();
  return (
    <Body
      color="foreground"
      onPress={useCallback(() => push('episodeDetail', { id: props.id! }), [props.id, push])}
    >
      {props.name}
    </Body>
  );
});

export default React.memo<AppScreenProps<'characterDetail'>>(
  ({
    route: {
      params: { id },
    },
  }) => {
    const characterDetail = useCharacterDetail(id);
    const [getEpisodes, characterEpisodes] = useCharacterEpisodes(id);
    const onGetEpisodes = useCallback(() => getEpisodes(), [getEpisodes]);
    return (
      <Screen title={characterDetail.data?.character?.name}>
        <CharacterProp>{characterDetail.data?.character}</CharacterProp>
        {characterEpisodes.data ? (
          <Body color="foreground">Episodes</Body>
        ) : (
          <Button title="Show episodes" onPress={onGetEpisodes}>
            Get episodes
          </Button>
        )}
        {characterEpisodes.data?.character?.episode.map(e => e && <Episode key={e.id} {...e} />)}
      </Screen>
    );
  }
);
