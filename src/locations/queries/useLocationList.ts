import { useCallback } from 'react';
import { FilterLocation } from '__generated__/globalTypes';
import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { LocationList, LocationListVariables } from './__generated__/LocationList';

const LOCATION_LIST = gql`
  query LocationList($page: Int, $filter: FilterLocation) {
    locations(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        type
        dimension
      }
    }
  }
`;

export const useLocationList = (filter?: FilterLocation) => {
  const { fetchMore, ...query } = useQuery<LocationList, LocationListVariables>(LOCATION_LIST, {
    variables: { filter },
  });
  const nextPage = query.data?.locations?.info?.next || 0;
  const hasMore = (query.data?.locations?.info?.pages || 0) > nextPage;
  return {
    ...query,
    fetchMore: useCallback(() => {
      if (!nextPage || !hasMore) return;
      void fetchMore({
        variables: { page: nextPage },
        updateQuery: (prev, { fetchMoreResult: { locations } = {} }) => {
          if (!locations) return prev;
          const result: LocationList = {
            locations: {
              __typename: 'Locations',
              info: locations.info,
              results: [...(prev.locations?.results || []), ...(locations.results || [])],
            },
          };
          return result;
        },
      });
    }, [fetchMore, hasMore, nextPage]),
    hasMore,
  };
};
