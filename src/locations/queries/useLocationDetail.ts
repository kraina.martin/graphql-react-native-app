import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { LocationDetail } from './__generated__/LocationDetail';

const query = gql`
  query LocationDetail($id: ID!) {
    location(id: $id) {
      id
      name
      type
      dimension
      residents {
        id
        name
        image
        species
        origin {
          id
          name
        }
        location {
          id
          name
        }
      }
    }
  }
`;

export const useLocationDetail = (id: string) =>
  useQuery<LocationDetail>(query, { variables: { id } });
