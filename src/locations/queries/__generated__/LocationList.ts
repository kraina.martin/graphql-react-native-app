/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FilterLocation } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: LocationList
// ====================================================

export interface LocationList_locations_info {
  __typename: "Info";
  /**
   * The length of the response.
   */
  count: number | null;
  /**
   * The amount of pages.
   */
  pages: number | null;
  /**
   * Number of the next page (if it exists)
   */
  next: number | null;
  /**
   * Number of the previous page (if it exists)
   */
  prev: number | null;
}

export interface LocationList_locations_results {
  __typename: "Location";
  /**
   * The id of the location.
   */
  id: string | null;
  /**
   * The name of the location.
   */
  name: string | null;
  /**
   * The type of the location.
   */
  type: string | null;
  /**
   * The dimension in which the location is located.
   */
  dimension: string | null;
}

export interface LocationList_locations {
  __typename: "Locations";
  info: LocationList_locations_info | null;
  results: (LocationList_locations_results | null)[] | null;
}

export interface LocationList {
  /**
   * Get the list of all locations
   */
  locations: LocationList_locations | null;
}

export interface LocationListVariables {
  page?: number | null;
  filter?: FilterLocation | null;
}
