import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';

import { Body } from '~src/ui';

import { LocationList_locations_results } from '../../queries';

import * as S from './styled';

const CharacterCard = React.memo<LocationList_locations_results>(props => {
  const { navigate } = useNavigation();
  return (
    <S.Wrapper
      onPress={useCallback(
        () => navigate('locationDetail', { id: props.id! }),
        [navigate, props.id]
      )}
    >
      <Body color="foreground">{props.name}</Body>
      <Body color="foreground">{props.dimension}</Body>
    </S.Wrapper>
  );
});

export default CharacterCard;
