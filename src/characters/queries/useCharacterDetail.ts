import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { CharacterDetail } from './__generated__/CharacterDetail';

const query = gql`
  query CharacterDetail($id: ID!) {
    character(id: $id) {
      id
      name
      status
      species
      type
      gender
      origin {
        id
        name
        dimension
      }
      location {
        id
        name
        dimension
      }
      image
      created
    }
  }
`;

export const useCharacterDetail = (id: string) =>
  useQuery<CharacterDetail>(query, { variables: { id } });
