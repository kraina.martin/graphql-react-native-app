import { useLazyQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { CharacterEpisodes } from './__generated__/CharacterEpisodes';

const query = gql`
  query CharacterEpisodes($id: ID!) {
    character(id: $id) {
      episode {
        id
        name
        air_date
      }
    }
  }
`;

export const useCharacterEpisodes = (id: string) =>
  useLazyQuery<CharacterEpisodes>(query, { variables: { id } });
