/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FilterCharacter } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: CharacterList
// ====================================================

export interface CharacterList_characters_info {
  __typename: "Info";
  /**
   * The length of the response.
   */
  count: number | null;
  /**
   * The amount of pages.
   */
  pages: number | null;
  /**
   * Number of the next page (if it exists)
   */
  next: number | null;
  /**
   * Number of the previous page (if it exists)
   */
  prev: number | null;
}

export interface CharacterList_characters_results_origin {
  __typename: "Location";
  /**
   * The id of the location.
   */
  id: string | null;
  /**
   * The name of the location.
   */
  name: string | null;
}

export interface CharacterList_characters_results_location {
  __typename: "Location";
  /**
   * The id of the location.
   */
  id: string | null;
  /**
   * The name of the location.
   */
  name: string | null;
}

export interface CharacterList_characters_results {
  __typename: "Character";
  /**
   * The id of the character.
   */
  id: string | null;
  /**
   * The name of the character.
   */
  name: string | null;
  /**
   * Link to the character's image.
   * All images are 300x300px and most are medium shots or portraits since they are intended to be used as avatars.
   */
  image: string | null;
  /**
   * The species of the character.
   */
  species: string | null;
  /**
   * The character's origin location
   */
  origin: CharacterList_characters_results_origin | null;
  /**
   * The character's last known location
   */
  location: CharacterList_characters_results_location | null;
}

export interface CharacterList_characters {
  __typename: "Characters";
  info: CharacterList_characters_info | null;
  results: (CharacterList_characters_results | null)[] | null;
}

export interface CharacterList {
  /**
   * Get the list of all characters
   */
  characters: CharacterList_characters | null;
}

export interface CharacterListVariables {
  page?: number | null;
  filter?: FilterCharacter | null;
}
