/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CharacterEpisodes
// ====================================================

export interface CharacterEpisodes_character_episode {
  __typename: "Episode";
  /**
   * The id of the episode.
   */
  id: string | null;
  /**
   * The name of the episode.
   */
  name: string | null;
  /**
   * The air date of the episode.
   */
  air_date: string | null;
}

export interface CharacterEpisodes_character {
  __typename: "Character";
  /**
   * Episodes in which this character appeared.
   */
  episode: (CharacterEpisodes_character_episode | null)[];
}

export interface CharacterEpisodes {
  /**
   * Get a specific character by ID
   */
  character: CharacterEpisodes_character | null;
}

export interface CharacterEpisodesVariables {
  id: string;
}
