import { useCallback } from 'react';
import { FilterCharacter } from '__generated__/globalTypes';
import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { CharacterList, CharacterListVariables } from './__generated__/CharacterList';

const CHARACTER_LIST = gql`
  query CharacterList($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        image
        species
        origin {
          id
          name
        }
        location {
          id
          name
        }
      }
    }
  }
`;

export const useCharacterList = (filter?: FilterCharacter) => {
  const { fetchMore, ...query } = useQuery<CharacterList, CharacterListVariables>(CHARACTER_LIST, {
    variables: { filter },
  });
  const nextPage = query.data?.characters?.info?.next || 0;
  const hasMore = (query.data?.characters?.info?.pages || 0) > nextPage;

  return {
    ...query,
    fetchMore: useCallback(() => {
      if (!nextPage || !hasMore) return;
      void fetchMore({
        variables: { page: nextPage },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult?.characters) return prev;
          const result: CharacterList = {
            characters: {
              __typename: 'Characters',
              info: fetchMoreResult.characters.info,
              results: [
                ...(prev.characters?.results || []),
                ...(fetchMoreResult.characters.results || []),
              ],
            },
          };
          return result;
        },
      });
    }, [fetchMore, hasMore, nextPage]),
    hasMore,
  };
};
