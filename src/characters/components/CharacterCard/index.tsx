import React, { useCallback } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { H1, H3 } from '~src/ui';

import { CharacterList_characters_results } from '../../queries';

import * as S from './styled';

const CharacterCard = React.memo<CharacterList_characters_results>(props => {
  const { navigate } = useNavigation();
  return (
    <S.Wrapper
      onPress={useCallback(
        () => navigate('characterDetail', { id: props.id! }),
        [navigate, props.id]
      )}
    >
      <S.CharacterImage uri={props.image || undefined} />
      <View>
        <H1 color="foreground">{props.name}</H1>
        <H3 color="foreground">{props.location?.name}</H3>
        <H3 color="foreground">{props.origin?.name}</H3>
        <H3 color="foreground">{props.species}</H3>
      </View>
    </S.Wrapper>
  );
});

export default CharacterCard;
