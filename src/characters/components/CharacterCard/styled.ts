import styled from 'styled-components/native';

import { Image, Touchable } from '~src/ui';

export const Wrapper = styled(Touchable)(({ theme: { colors, isDark } }) => ({
  backgroundColor: isDark ? colors.background01 : colors.background,
  margin: 8,
  marginHorizontal: 8,
  padding: 8,
  flexDirection: 'row',
  borderWidth: 1,
  borderColor: isDark ? colors.background02 : colors.background02,
  borderRadius: 360,
  overflow: 'hidden',
  alignItems: 'center',
}));

export const CharacterImage = styled(Image)({ height: 115, width: 115, borderRadius: 360 });
