export interface Theme {
  borderRadius: {
    circle: number;
    edge: number;
    rounded: number;
  };
  colors: {
    background: string;
    background01: string;
    background02: string;
    foreground: string;
    foreground01: string;
    foreground02: string;
    primary: string;
    primary01: string;
    primary02: string;
  };
  fontSize: {
    extraSmall: number;
    large: number;
    medium: number;
    regular: number;
    small: number;
  };
  isDark: boolean;
  spacing: {
    large: number;
    medium: number;
    small: number;
  };
}
