import React, { useMemo } from 'react';
import { useColorScheme } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { ThemeProvider } from 'styled-components/native';

import StatusBar from './components/StatusBar';
import { Theme } from './types';

const lightTheme: Pick<Theme, 'colors' | 'isDark'> = {
  isDark: false,
  colors: {
    background: 'rgb(255, 255, 255)',
    background01: 'rgb(240, 240, 240)',
    background02: 'rgb(183,183,183)',
    foreground02: 'rgb(135, 135, 135)',
    foreground01: 'rgb(40, 40, 40)',
    foreground: 'rgb(14,14,14)',
    primary: 'rgb(52, 191, 5)',
    primary01: 'rgb(0, 150, 0)',
    primary02: 'rgb(107, 215, 70)',
  },
};
const darkTheme: Pick<Theme, 'colors' | 'isDark'> = {
  isDark: true,
  colors: {
    foreground: 'rgb(255, 255, 255)',
    foreground01: 'rgb(183,183,183)',
    foreground02: 'rgb(135, 135, 135)',
    background: 'rgb(14,14,14)',
    background01: 'rgb(25, 25 , 25)',
    background02: 'rgb(50, 50, 50)',
    primary: 'rgb(52, 191, 5)',
    primary01: 'rgb(107, 215, 70)',
    primary02: 'rgb(0, 150, 0)',
  },
};

const themeBase: Omit<Theme, 'colors' | 'isDark'> = {
  fontSize: { large: 32, medium: 24, regular: 16, small: 14, extraSmall: 12 },
  borderRadius: {
    circle: 50,
    rounded: 30,
    edge: 4,
  },
  spacing: { large: 16, medium: 8, small: 4 },
};

export const UiProvider = React.memo<{ children: React.ReactNode }>(({ children }) => {
  const colorScheme = useColorScheme();
  return (
    <SafeAreaProvider>
      <ThemeProvider
        theme={useMemo(
          () => ({ ...themeBase, ...(colorScheme === 'dark' ? darkTheme : lightTheme) }),
          [colorScheme]
        )}
      >
        <StatusBar />
        {children}
      </ThemeProvider>
    </SafeAreaProvider>
  );
});
