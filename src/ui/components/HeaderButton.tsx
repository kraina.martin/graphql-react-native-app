import React from 'react';
import styled from 'styled-components/native';

import { Icon, IconName } from './Icon';

export interface HeaderButtonProps {
  icon: IconName;
  onPress: () => void;
}

const Button = styled(Icon)({
  padding: 8,
});

export const HeaderButton = React.memo<HeaderButtonProps>(props => {
  const { onPress, icon } = props;

  return <Button size={22} color="foreground" name={icon} onPress={onPress} />;
});
