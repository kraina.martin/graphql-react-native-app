import React from 'react';
import { ActivityIndicator, StyleProp, ViewStyle } from 'react-native';
import { useTheme } from 'styled-components/native';

type Props = {
  style?: StyleProp<ViewStyle>;
};

const LoadingIndicator = React.memo<Props>(({ style }) => {
  const { colors } = useTheme();
  return <ActivityIndicator size="large" style={style} color={colors.primary} />;
});

export default LoadingIndicator;
