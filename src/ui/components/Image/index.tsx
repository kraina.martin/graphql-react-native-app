import React, { useCallback, useMemo, useState } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { FastImageProps, OnLoadEvent, Priority } from 'react-native-fast-image';

import placeholderImage from '~assets/logo.png';

import * as S from './styled';

interface Props extends Omit<FastImageProps, 'source' | 'style'> {
  uri: string | number | undefined;
  priority?: Priority;
  style?: StyleProp<ViewStyle>;
  withPlaceholder?: boolean;
}

const Image = React.memo<Props>(
  ({
    uri,
    withPlaceholder = typeof uri !== 'number',
    onLoadEnd,
    onError,
    onLoad,
    style,
    priority,
    ...props
  }) => {
    const [isLoaded, setIsLoaded] = useState(false);
    return (
      <S.Wrapper style={style}>
        {!isLoaded && withPlaceholder && <S.Image source={placeholderImage} opacity={0.5} />}
        <S.Image
          {...props}
          source={useMemo(
            () => (typeof uri === 'number' ? uri : { uri, priority }),
            [priority, uri]
          )}
          onError={onError}
          onLoadEnd={onLoadEnd}
          onLoad={useCallback(
            (e: OnLoadEvent) => {
              setIsLoaded(true);
              onLoad?.(e);
            },
            [onLoad]
          )}
        />
      </S.Wrapper>
    );
  }
);

export default Image;
