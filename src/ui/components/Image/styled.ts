import FastImage from 'react-native-fast-image';
import styled from 'styled-components/native';

export const Wrapper = styled.View({ overflow: 'hidden' });
export const Image = styled(FastImage)<{ opacity?: number }>(({ opacity }) => ({
  position: 'absolute',
  top: 0,
  right: 0,
  left: 0,
  bottom: 0,
  opacity,
}));
