import styled from 'styled-components/native';

export const Inset = styled.View<{ size: number }>(({ size }) => ({
  height: size,
}));
