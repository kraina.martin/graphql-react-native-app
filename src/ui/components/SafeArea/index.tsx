import React from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import * as S from './styled';

const Top = React.memo(() => <S.Inset size={useSafeAreaInsets().top} />);
const Bottom = React.memo(() => <S.Inset size={useSafeAreaInsets().bottom} />);

export default { Top, Bottom };
