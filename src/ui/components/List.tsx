import React, { useMemo } from 'react';
import { FlatList, FlatListProps, RefreshControl, StyleSheet } from 'react-native';
import { useTheme } from 'styled-components/native';

import ContentLoadIndicator from './ContentLoadIndicator';
import LoadingIndicator from './LoadingIndicator';

const styles = StyleSheet.create({
  listEmptyContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export type ListItemBase = { id: string | null };

export type ListProps<T extends ListItemBase> = Omit<
  FlatListProps<T>,
  'keyExtractor' | 'ListEmptyComponent' | 'data' | 'refreshControl'
> & {
  data: (T | null)[] | null | undefined;
  hasMore?: boolean;
};

const keyExtractor = (item: ListItemBase, index: number) => `${item.id || index}`;

const List = React.memo(
  React.forwardRef<FlatList, ListProps<ListItemBase>>(
    ({ data, contentContainerStyle, hasMore, refreshing, onRefresh, ...props }, ref) => {
      const { colors } = useTheme();
      const refreshColors = useMemo(() => [colors.foreground], [colors.foreground]);
      const hasData = !!data;
      const isRefreshing = !!(refreshing && hasData);
      return (
        <FlatList
          ref={ref}
          {...props}
          refreshControl={useMemo(
            () =>
              onRefresh ? (
                <RefreshControl
                  colors={refreshColors}
                  tintColor={refreshColors[0]}
                  progressBackgroundColor={colors.background}
                  refreshing={isRefreshing}
                  onRefresh={onRefresh}
                />
              ) : undefined,
            [colors.background, isRefreshing, onRefresh, refreshColors]
          )}
          data={useMemo(() => data?.filter((r): r is ListItemBase => !!r), [data])}
          keyExtractor={keyExtractor}
          contentContainerStyle={data?.length ? contentContainerStyle : styles.listEmptyContainer}
          ListEmptyComponent={useMemo(
            () => (
              <ContentLoadIndicator loading={!!refreshing} />
            ),
            [refreshing]
          )}
          ListFooterComponent={useMemo(
            () => (hasMore && hasData ? <LoadingIndicator /> : null),
            [hasData, hasMore]
          )}
        />
      );
    }
  )
);
export default List as <T extends ListItemBase>(
  props: ListProps<T> & { ref?: React.ForwardedRef<FlatList> }
) => React.ReactElement;
