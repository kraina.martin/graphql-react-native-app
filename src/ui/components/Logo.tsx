import React from 'react';
import { Image as RNImage, StyleProp, ViewStyle } from 'react-native';
import styled from 'styled-components/native';

import Logo from '~assets/logo.png';
import LogoHorizontal from '~assets/logoHorizontal.png';

import Image from './Image';

interface Props {
  height: number;
  horizontal?: boolean;
  style?: StyleProp<ViewStyle>;
}
const horizontalLogoSize = RNImage.resolveAssetSource(LogoHorizontal);
const horizontalLogoAspectRatio = `${horizontalLogoSize.width / horizontalLogoSize.height}`;
const logoSize = RNImage.resolveAssetSource(LogoHorizontal);
const logoAspectRatio = `${logoSize.width / logoSize.height}`;
const StyledImage = React.memo(
  styled(Image)<Pick<Props, 'height'> & { ratio: string }>(({ height, ratio }) => ({
    height,
    aspectRatio: ratio,
  }))
);

export default React.memo<Props>(({ horizontal, ...props }) => {
  return (
    <StyledImage
      {...props}
      ratio={horizontal ? horizontalLogoAspectRatio : logoAspectRatio}
      uri={horizontal ? LogoHorizontal : Logo}
      resizeMode="contain"
    />
  );
});
