import React, { useMemo } from 'react';
import {
  TouchableNativeFeedback,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';
import { useTheme } from 'styled-components/native';

import env from '~src/env';

import { Color } from '../utils';

export type TouchableProps = Omit<TouchableOpacityProps, 'testID'> & {
  children?: React.ReactNode;
  color?: string;
  noFeedback?: true;
};

const TouchableAndroid = React.memo<TouchableProps>(props => {
  const { colors } = useTheme();
  const { color = colors.foreground, style, children, ...restProps } = props;

  return (
    <TouchableNativeFeedback
      {...restProps}
      useForeground
      background={useMemo(
        () =>
          TouchableNativeFeedback.Ripple(
            Color.alpha(color, Color.isDark(color) ? 0.1 : 0.25),
            true
          ),
        [color]
      )}
    >
      <View style={useMemo(() => [{ overflow: 'hidden' }, style], [style])}>{children}</View>
    </TouchableNativeFeedback>
  );
});

const Touchable = React.memo<TouchableProps>(props => {
  const disabled =
    props.disabled ||
    !(
      props.onPress ||
      props.onPressIn ||
      props.onPressOut ||
      props.onLongPress ||
      props.onMagicTap
    );

  if (env.isIPhone || props.noFeedback)
    return (
      <TouchableOpacity {...props} disabled={disabled} activeOpacity={props.noFeedback && 1}>
        {props.children}
      </TouchableOpacity>
    );
  return <TouchableAndroid {...props} disabled={disabled} />;
});

export default Touchable;
