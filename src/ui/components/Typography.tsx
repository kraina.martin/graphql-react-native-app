/* eslint-disable react-memo/require-memo */
import { StyleProp, TextProps as RNTextProps, TextStyle } from 'react-native';
import styled from 'styled-components/native';

import { Theme } from '../types';

// from OS user preference - when enabling, make sure it won't break the UI
const ALLOW_FONT_SCALING = false;

interface Props extends Omit<RNTextProps, 'style'> {
  align?: 'left' | 'right' | 'center';
  bold?: boolean;
  color?: keyof Theme['colors'];
  style?: StyleProp<Omit<TextStyle, 'fontWeight'>>;
  transform?: TextStyle['textTransform'];
}

const Default = styled.Text.attrs(({ allowFontScaling = ALLOW_FONT_SCALING }) => ({
  allowFontScaling,
}))<Props>(({ theme, align, color = 'foreground', transform, bold }) => ({
  color: theme.colors[color],
  textAlign: align,
  textTransform: transform,
  fontWeight: bold ? 'bold' : 'normal',
}));

type BodyProps = { color: keyof Theme['colors'] };
export const Body = styled(Default)<BodyProps>(({ theme, color = 'foreground' }) => ({
  color: theme.colors[color],
  fontSize: theme.fontSize.regular,
}));

export type TextProps = Props & BodyProps;

// Heading
export const H1 = styled(Body)(({ theme }) => ({ fontSize: theme.fontSize.large }));
export const H2 = styled(Body)(({ theme }) => ({ fontSize: theme.fontSize.medium }));
export const H3 = styled(Body)(({ theme }) => ({ fontSize: theme.fontSize.regular }));
export const H4 = styled(Body)(({ theme }) => ({ fontSize: theme.fontSize.small }));
export const H5 = styled(Body)(({ theme }) => ({ fontSize: theme.fontSize.extraSmall }));
