import React, { useMemo } from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import glyphMap from 'react-native-vector-icons/glyphmaps/FontAwesome5Free.json';
import styled, { useTheme } from 'styled-components/native';

import { Theme } from '../types';

import Touchable, { TouchableProps } from './Touchable';

const Wrapper = styled(Touchable)({
  borderRadius: 360,
  alignItems: 'center',
  justifyContent: 'center',
});

export type IconName = keyof typeof glyphMap;
export interface IconProps extends Omit<TouchableProps, 'onLayout' | 'children'> {
  color: keyof Theme['colors'];
  name: IconName;
  size: number;
}

export const Icon = React.memo<IconProps>(props => {
  const { style, name, color, size, ...touchableProps } = props;
  const { colors } = useTheme();
  return (
    <Wrapper {...touchableProps} color={colors[color]} style={style}>
      <FontAwesome5
        name={name}
        color={colors[color]}
        size={size}
        style={useMemo(() => [{ lineHeight: size }], [size])}
      />
    </Wrapper>
  );
});
