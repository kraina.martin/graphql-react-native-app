import React from 'react';
import { StatusBar as RNStatusBar } from 'react-native';
import { useTheme } from 'styled-components/native';

import env from '~src/env';

interface Props {
  hidden?: boolean;
}

const transparentBackgroundSupported = env.isIPhone || env.osVersion >= 23;

const StatusBar = React.memo<Props>(({ hidden = false }) => {
  const darkContent = !useTheme().isDark;

  return (
    <RNStatusBar
      barStyle={darkContent && transparentBackgroundSupported ? 'dark-content' : 'light-content'}
      translucent={transparentBackgroundSupported}
      backgroundColor={transparentBackgroundSupported ? 'transparent' : 'black'}
      hidden={hidden}
    />
  );
});

export default StatusBar;
