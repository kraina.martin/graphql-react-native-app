import React from 'react';
import { ApolloError } from '@apollo/client';
import styled from 'styled-components/native';

import LoadingIndicator from './LoadingIndicator';

const Wrapper = styled.View({
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
});

type Props = {
  loading: boolean;
  error?: ApolloError;
  refetch?: () => void;
};

const ContentLoadingIndication = React.memo<Props>(({ loading }) => {
  //TODO: handle error & refetch
  return <Wrapper>{loading ? <LoadingIndicator /> : null}</Wrapper>;
});

export default ContentLoadingIndication;
