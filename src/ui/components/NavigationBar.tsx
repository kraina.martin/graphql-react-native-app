import React, { useEffect } from 'react';
import { Platform } from 'react-native';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Color from 'color';

import env from '~src/env';

interface NavigationBarProps {
  color: string;
}

const history: Color<string>[] = [];

const NavigationBar = React.memo<NavigationBarProps>(props => {
  useEffect(() => {
    if (env.isIPhone) return;
    const color = Color(Platform.Version < 26 ? 'rgb(0, 0, 0)' : props.color);

    changeNavigationBarColor(color.hex(), color.isLight(), true);
    history.push(color);

    return () => {
      history.pop();
      const previousColor = history[history.length - 1];

      if (!previousColor) return;
      changeNavigationBarColor(previousColor.hex(), previousColor.isLight(), true);
    };
  }, [props.color]);

  return null;
});

export default NavigationBar;
