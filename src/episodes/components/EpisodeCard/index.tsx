import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';

import { Body } from '~src/ui';

import { EpisodeList_episodes_results } from '../../queries';

import * as S from './styled';

const CharacterCard = React.memo<EpisodeList_episodes_results>(props => {
  const { navigate } = useNavigation();
  return (
    <S.Wrapper
      onPress={useCallback(
        () => navigate('episodeDetail', { id: props.id! }),
        [navigate, props.id]
      )}
    >
      <Body color="foreground">{props.name}</Body>
      <Body color="foreground">{props.episode}</Body>
    </S.Wrapper>
  );
});

export default CharacterCard;
