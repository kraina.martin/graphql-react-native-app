/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: EpisodeDetail
// ====================================================

export interface EpisodeDetail_episode_characters_origin {
  __typename: "Location";
  /**
   * The id of the location.
   */
  id: string | null;
  /**
   * The name of the location.
   */
  name: string | null;
}

export interface EpisodeDetail_episode_characters_location {
  __typename: "Location";
  /**
   * The id of the location.
   */
  id: string | null;
  /**
   * The name of the location.
   */
  name: string | null;
}

export interface EpisodeDetail_episode_characters {
  __typename: "Character";
  /**
   * The id of the character.
   */
  id: string | null;
  /**
   * The name of the character.
   */
  name: string | null;
  /**
   * Link to the character's image.
   * All images are 300x300px and most are medium shots or portraits since they are intended to be used as avatars.
   */
  image: string | null;
  /**
   * The species of the character.
   */
  species: string | null;
  /**
   * The character's origin location
   */
  origin: EpisodeDetail_episode_characters_origin | null;
  /**
   * The character's last known location
   */
  location: EpisodeDetail_episode_characters_location | null;
}

export interface EpisodeDetail_episode {
  __typename: "Episode";
  /**
   * The id of the episode.
   */
  id: string | null;
  /**
   * The name of the episode.
   */
  name: string | null;
  /**
   * The air date of the episode.
   */
  air_date: string | null;
  /**
   * The code of the episode.
   */
  episode: string | null;
  /**
   * List of characters who have been seen in the episode.
   */
  characters: (EpisodeDetail_episode_characters | null)[];
}

export interface EpisodeDetail {
  /**
   * Get a specific episode by ID
   */
  episode: EpisodeDetail_episode | null;
}

export interface EpisodeDetailVariables {
  id: string;
}
