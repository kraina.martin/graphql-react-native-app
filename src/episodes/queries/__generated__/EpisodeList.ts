/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FilterEpisode } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: EpisodeList
// ====================================================

export interface EpisodeList_episodes_info {
  __typename: "Info";
  /**
   * The length of the response.
   */
  count: number | null;
  /**
   * The amount of pages.
   */
  pages: number | null;
  /**
   * Number of the next page (if it exists)
   */
  next: number | null;
  /**
   * Number of the previous page (if it exists)
   */
  prev: number | null;
}

export interface EpisodeList_episodes_results {
  __typename: "Episode";
  /**
   * The id of the episode.
   */
  id: string | null;
  /**
   * The name of the episode.
   */
  name: string | null;
  /**
   * The air date of the episode.
   */
  air_date: string | null;
  /**
   * The code of the episode.
   */
  episode: string | null;
}

export interface EpisodeList_episodes {
  __typename: "Episodes";
  info: EpisodeList_episodes_info | null;
  results: (EpisodeList_episodes_results | null)[] | null;
}

export interface EpisodeList {
  /**
   * Get the list of all episodes
   */
  episodes: EpisodeList_episodes | null;
}

export interface EpisodeListVariables {
  page?: number | null;
  filter?: FilterEpisode | null;
}
