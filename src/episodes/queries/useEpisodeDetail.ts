import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { EpisodeDetail } from './__generated__';

const query = gql`
  query EpisodeDetail($id: ID!) {
    episode(id: $id) {
      id
      name
      air_date
      episode
      characters {
        id
        name
        image
        species
        origin {
          id
          name
        }
        location {
          id
          name
        }
      }
    }
  }
`;

export const useEpisodeDetail = (id: string) =>
  useQuery<EpisodeDetail>(query, { variables: { id } });
