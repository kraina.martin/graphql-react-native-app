import { useCallback } from 'react';
import { FilterEpisode } from '__generated__/globalTypes';
import { useQuery } from '@apollo/client';
import gql from 'graphql-tag';

import { EpisodeList, EpisodeListVariables } from './__generated__';

const EPISODE_LIST = gql`
  query EpisodeList($page: Int, $filter: FilterEpisode) {
    episodes(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        air_date
        episode
      }
    }
  }
`;

export const useEpisodeList = (filter?: FilterEpisode) => {
  const { fetchMore, ...query } = useQuery<EpisodeList, EpisodeListVariables>(EPISODE_LIST, {
    variables: { filter },
  });
  const nextPage = query.data?.episodes?.info?.next || 0;
  const hasMore = (query.data?.episodes?.info?.pages || 0) > nextPage;

  return {
    ...query,
    fetchMore: useCallback(() => {
      if (!nextPage || !hasMore) return;
      void fetchMore({
        variables: { page: nextPage },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult?.episodes) return prev;
          const result: EpisodeList = {
            episodes: {
              __typename: 'Episodes',
              info: fetchMoreResult.episodes.info,
              results: [
                ...(prev.episodes?.results || []),
                ...(fetchMoreResult.episodes.results || []),
              ],
            },
          };
          return result;
        },
      });
    }, [fetchMore, hasMore, nextPage]),
    hasMore,
  };
};
