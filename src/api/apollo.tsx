import React from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

export const apolloCache = new InMemoryCache();

const createApolloClient = () => {
  const apolloClient = new ApolloClient({
    uri: 'https://rickandmortyapi.com/graphql',
    connectToDevTools: process.env.NODE_ENV !== 'production',
    cache: apolloCache,
    assumeImmutableResults: true,
  });

  return apolloClient;
};

export const apolloClient = createApolloClient();

export default React.memo(({ children }) => (
  <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
));
