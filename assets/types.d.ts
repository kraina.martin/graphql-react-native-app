/* eslint-disable import/no-duplicates, import/newline-after-import */

declare module '*.json';

declare module '*.svg' {
  import { SvgProps } from 'react-native-svg';
  const content: React.FC<SvgProps>;
  export default content;
}

declare module '*.png' {
  import { ImageRequireSource } from 'react-native';
  const image: ImageRequireSource;
  export default image;
}

declare module '*.jpg' {
  import { ImageRequireSource } from 'react-native';
  const image: ImageRequireSource;
  export default image;
}
